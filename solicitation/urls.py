from django.contrib import admin
from django.urls import path,include
from . import views
app_name='solicitation'
urlpatterns = [
    path('',views.index, name='index'),
    path('projectowneradd',views.projectowneradd, name='projectoweneradd'),
    path('projectengineeradd', views.projectengineeradd, name='projectengineeradd'),
 	path('plankeeperadd', views.plankeeperadd, name='plankeeperadd'),
 	path('addsolicitationcontact', views.addsolicitationcontact, name='addsolicitationcontact'),
 	path('manageradd', views.manageradd, name='manageradd'),
 	path('storesolicdata', views.storesolicdata, name='storesolicdata'),

 	path('edit',views.solicitationlist, name='solicitationlist'),
 	path('edit/<int:pk>/', views.editsolicitation, name='editsolicitation'),
 	# path('<int:notenum>', views.viewnotes, name='viewnotes'),
]
