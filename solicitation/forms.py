from django import forms
from .models import Project, ProjectTermContract

class TermContractForm(forms.ModelForm):
	class Meta:
		model = ProjectTermContract
		fields = [
		'id',
		'current_start',
		'current_stop',
		'orig_start',
		'orig_stop',
		'contract_term',
		'ext_count',
		'ext_length',
		'status',
		]
class ProjectForm(forms.ModelForm):
	# required_source_url = forms.CharField(max_length=150)
	# name = ownerprjname
	class Meta:
		model = Project
		fields = [
		'source',
		'country',
		'stateprov',
		'county',
		'biddate',
		'bidtime',
		'presolicitation',
		'incomplete',
		'tags',
		'classname',
		'sourcetype',

		'ownerprjname',
		'ownerprjno',
		'location',
		'lowvalue',
		'highvalue',
		'bond',
		'prebiddate',
		'prebidtime',
		'prebidmandatory',
		'codedid',
		# 'status',

		'scope',
		'no_adr',
		'plans',
		'planprice',
		'outsidelink',
		'notes',
		'reporternotes',

		'owner',
		'ownercontact',

		'engineer',
		'engineercontact',
		
		'plan',
		'plancontact',
		'planphone',
		'planphoneext',
		'planfax',
		'planemail',

		'prime',
		'primecontact',
		'leadby',
		'reportby',
		'updatedby',
		
		'worktype',

		'region',
		'name',

		'adr_request_from_ph',
		'adr_request_from_bt',
		'adr_request_from_ca',
		'tags',
		'updatenotes',
		]

