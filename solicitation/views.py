from django.shortcuts import render
from . models import *
from django.core.paginator import Paginator
from dashpage.views import updateuserlogin
from django.http import HttpResponse
from .forms import ProjectForm
from . import otherfunctions
import ast
from .forms import TermContractForm
from django.contrib import messages


numberinpages = 10

def index(request):
	context = {
		'bidtimes':otherfunctions.bidtimes,
		# 'countries':Country.objects.all(),
		# 'states':Stateprov.objects.all(),
		# 'countys':County.objects.all(),
		'projectclasses':ProjectClass.objects.all().order_by('classname'),
		'locations':Location.objects.all().order_by('name'),
		'sources':Sourcetype.objects.all().order_by('name'),
		# 'worktypes':Worktype.objects.all(),
		# 'worktypegroups':Worktypegroup.objects.all(),
		'projectstatus':ProjectStatus.objects.all(),

		'utilityconstructions':otherfunctions.utilityconstructions,
		'BuildingTrades':otherfunctions.BuildingTrades,
		'RoadpavingandBridges':otherfunctions.RoadpavingandBridges,
		'architecturalengineeringconsultings':otherfunctions.architecturalengineeringconsultings,
		'SpecialtyTrades':otherfunctions.SpecialtyTrades,
		'OtherServices':otherfunctions.OtherServices,
		'ItemSaleLeaseTrades':otherfunctions.ItemSaleLeaseTrades,
		'ConstructionMaterials':otherfunctions.ConstructionMaterials,
		'PurchaseRentalLeaseofGoods':otherfunctions.PurchaseRentalLeaseofGoods,
		'EquipmentMachineryVehicles':otherfunctions.EquipmentMachineryVehicles,
	}
	# updateuserlogin(request.user.username)
	return render(request,'solicitation/solic_add.html',context)

def projectowneradd(request):
	areacode = request.GET.get('areacode')
	if areacode is None:
		countries = Country.objects.all()
		states = Stateprov.objects.all()
		entity_types = Entitytype.objects.all().order_by('typename')
		context = {
			'countries':countries,
			'states':states,
			'entity_types':entity_types
		}
		return render(request,'solicitation/entities/projectownerselector.html',context)
	else:
		country = request.GET.get('country')
		state = request.GET.get('state')
		phonenumber = request.GET.get('phonenumber')
		city = request.GET.get('city')
		zipcode = request.GET.get('zipcode')
		entitytype = request.GET.get('entitytype')
		ownertype = request.GET.get('ownertype')

		entities = Entity.objects.all().filter(entitytype=5)
		if country is not None and (len(country)>0):
			entities = entities.filter(country=country)
		if state is not None and (len(state)>0):
			entities = entities.filter(stateprov_id=state)
		if phonenumber is not None and (len(phonenumber)>0):
			entities = entities.filter(phone__contains=phonenumber)
		if city is not None and (len(city)>0):
			entities = entities.filter(city=city)
		if zipcode is not None and (len(zipcode)>0):
			entities = entities.filter(zip=zipcode)
		if entitytype is not None and (len(entitytype)>0):
			entities = entities.filter(entitytype_id=entitytype)

		paginator = Paginator(entities,numberinpages)
		page = request.GET.get('page')
		entity_obj = paginator.get_page(page)

		context = {
			'areacode':areacode,
			'entities':entity_obj,
			'state':state,
			'country':country,
			'phonenumber':phonenumber,
			'zipcode': zipcode,
			'city':city,
			'entitytype':entitytype,
			'ownertype':ownertype,
		}
		return render(request, 'solicitation/entities/projectowneradd.html',context)

def projectengineeradd(request):
	country = request.GET.get('country')
	if country is None:
		countries = Country.objects.all()
		states = Stateprov.objects.all()
		context = {
		'countries':countries,
		'states':states,
		}
		return render(request,'solicitation/entities/projectengineerselector.html',context)
	else:
		# page = 	request.GET['page']
		country = request.GET.get('country')
		state = request.GET.get('state')
		phonenumber = request.GET.get('phonenumber')
		city = request.GET.get('city')
		zipcode = request.GET.get('zipcode')
		entities = Entity.objects.all().filter(entitytype=4)
		if country is not None and (len(country)>0):
			entities = entities.filter(country=country)
		if state is not None and (len(state)>0):
			entities = entities.filter(stateprov_id=state)
		if phonenumber is not None and (len(phonenumber)>0):
			entities = entities.filter(phone__contains=phonenumber)
		if city is not None and (len(city)>0):
			entities = entities.filter(city=city)
		if zipcode is not None and (len(zipcode)>0):
			entities = entities.filter(zip__contains=zipcode)

		paginator = Paginator(entities,numberinpages)
		page = request.GET.get('page')
		entity_obj = paginator.get_page(page)

		context = {
			'entities':entity_obj,
			'country':country,
			'state':state,
			'phonenumber':phonenumber,
			'city':city,
			'zipcode':zipcode,
		}
		return render(request, 'solicitation/entities/projectengineeradd.html',context)

def plankeeperadd(request):
	country = request.GET.get('country')
	if country is None:
		countries = Country.objects.all()
		states = Stateprov.objects.all()
		context = {
		'countries':countries,
		'states':states,
		}
		return render(request,'solicitation/entities/plankeeperselector.html', context)
	else:
		# page = 	request.GET['page']
		country = request.GET.get('country')
		state = request.GET.get('state')
		phonenumber = request.GET.get('phonenumber')
		city = request.GET.get('city')
		zipcode = request.GET.get('zipcode')
		entities = Entity.objects.all().filter(entitytype=3)
		if country is not None and (len(country)>0):
			entities = entities.filter(country=country)
		if state is not None and (len(state)>0):
			entities = entities.filter(stateprov_id=state)
		if phonenumber is not None and (len(phonenumber)>0):
			entities = entities.filter(phone__contains=phonenumber)
		if city is not None and (len(city)>0):
			entities = entities.filter(city=city)
		if zipcode is not None and (len(zipcode)>0):
			entities = entities.filter(zip__contains=zipcode)

		paginator = Paginator(entities,numberinpages)
		page = request.GET.get('page')
		entity_obj = paginator.get_page(page)

		context = {
			'entities':entity_obj,
			'country':country,
			'state':state,
			'phonenumber':phonenumber,
			'city':city,
			'zipcode':zipcode,
		}
		return render(request,'solicitation/entities/plankeeperadd.html',context)

def manageradd(request):
	state= request.GET.get('state')
	if state is None:
		states = Stateprov.objects.all()
		entity_types = Entitytype.objects.all()
		context = {
			'states':states,
			'entity_types':entity_types,
		}
		return render(request,'solicitation/entities/managerselector.html', context)
	else:
		phone = request.GET.get('phone')
		entitytype = request.GET.get('entitytype')
		state = request.GET.get('state')
		entityname = request.GET.get('entityname')
		entities = Entity.objects.all()
		if phone is not None and (len(phone)>0):
			entities = entities.filter(phone__contains=phone)
		if entitytype is not None and (len(entitytype)>0):
			entities = entities.filter(entitytype_id=entitytype)
		if state is not None and (len(state)>0):
			entities = entities.filter(stateprov_id=state)
		if entityname is not None and (len(entityname)>0):
			entities = entities.filter(entityname=entityname)

		paginator = Paginator(entities,numberinpages)
		page = request.GET.get('page')
		entity_obj = paginator.get_page(page)
		context = {
			'entities':entity_obj,
			'entityname':entityname,
			'phone':phone,
			'entitytype':entitytype,
			'state':state,
		}
		return render(request,'solicitation/entities/manageradd.html',context)

def addsolicitationcontact(request):
	solicitor = request.GET['solicotor']
	entity_id = request.GET['entityid']
	contact_id = request.GET['contactid']
	if entity_id is None or entity_id == '':
		return HttpResponse('Please select entity first')
	solicitorname = Entity.objects.get(entity_id=entity_id)
	solicitorname = solicitorname.entityname
	if contact_id is None or contact_id == 'None' or contact_id=='':
		contacts = Contact.objects.all()
		messages.info(request, 'Showing all contacts')
	else:
		contacts = Contact.objects.filter(contact_id=contact_id)
		if len(contacts)==0:
			contacts = Contact.objects.all()
			messages.info(request, 'Contact num: '+contact_id+' not found. Showing all contacts')

	paginator = Paginator(contacts,numberinpages)
	page = request.GET.get('page')
	contacts = paginator.get_page(page)

	context = {
	'solicitorname':solicitorname,
	'solicitor':solicitor,
	'entityid':entity_id,
	'contactid':contact_id,
	'contacts':contacts,
	}
	return render(request,'solicitation/entities/addsolicitorcontact.html',context)

def storesolicdata(request):
	projectrequest = otherfunctions.preparedata(request)
	form = ProjectForm(projectrequest)
	if form.is_valid():
		savedform = form.save()
		otherfunctions.savesetaside(request.POST.getlist('setaside'),savedform.pk)
		otherfunctions.savetermcontract(request,savedform.pk)
		return HttpResponse('project saved')
	else:
		print('form error')
		return HttpResponse(otherfunctions.convertformerror(form.errors))

def solicitationlist(request):
	projects = Project.objects.all()
	paginator = Paginator(projects,numberinpages)
	page = request.GET.get('page')
	projects = paginator.get_page(page)
	messages.info(request, 'Showing '+str(numberinpages)+' results')
	return render(request,'solicitation/solicitationlist.html',{'projects':projects})

def editsolicitation(request,pk):
	if request.method == 'POST':
		request.POST = otherfunctions.preparedata(request)
		form = ProjectForm(instance=Project.objects.get(pk=pk),data=request.POST)
		if form.is_valid():
			savedform = form.save()
			otherfunctions.deletesetaside(pk)
			otherfunctions.savesetaside(request.POST.getlist('setaside'),savedform.pk)
			otherfunctions.edittermcontract(request,savedform.pk)
			return HttpResponse('updated')
		else:
			return HttpResponse(form.errors)
	context = {
		'project':Project.objects.get(pk=pk),
		'countries':Country.objects.all(),
		'states' : Stateprov.objects.all(),
		'countys' : County.objects.all(),
		'projectclasses':ProjectClass.objects.all().order_by('classname'),
		'sources':Sourcetype.objects.all().order_by('name'),
		'locations':Location.objects.all().order_by('name'),
		'setasides':ProjectSetAside.objects.filter(prjid=pk).values_list('setasidetypeid',flat=True),
		'numberoflinks': ast.literal_eval(Project.objects.get(pk=pk).outsidelink),
		
		'termcontract':ProjectTermContract.objects.get(pk=pk),
		'projectstatus':ProjectStatus.objects.all(),

		'bidtimes':otherfunctions.bidtimes,
		'utilityconstructions':otherfunctions.utilityconstructions,
		'BuildingTrades':otherfunctions.BuildingTrades,
		'RoadpavingandBridges':otherfunctions.RoadpavingandBridges,
		'architecturalengineeringconsultings':otherfunctions.architecturalengineeringconsultings,
		'SpecialtyTrades':otherfunctions.SpecialtyTrades,
		'OtherServices':otherfunctions.OtherServices,
		'ItemSaleLeaseTrades':otherfunctions.ItemSaleLeaseTrades,
		'ConstructionMaterials':otherfunctions.ConstructionMaterials,
		'PurchaseRentalLeaseofGoods':otherfunctions.PurchaseRentalLeaseofGoods,
		'EquipmentMachineryVehicles':otherfunctions.EquipmentMachineryVehicles,
	}
	return render(request,'solicitation/edit-solicitation/edit-solicitation.html',context)