from django.test import TestCase

def storesolicdata(request):
	project = Project()
	''' Required field '''
	project.source = request.POST['required_source_url']
	project.country =  Country.objects.get(country_id=request.POST['required_country_name'])
	project.biddate = request.POST['required_bid_date']#required_bid_date
	# project.tags = request.POST['required_bidtype']#required_bidtype
	project.bidtime = request.POST['required_bid_time']#required_bid_time
	project.classname = ProjectClass.objects.get(classname_id=request.POST['required_class'])
	project.sourcetype = Sourcetype.objects.get(sourcetypeid=request.POST['required_source']) 
	project.stateprov = Stateprov.objects.get(stateprov_id=request.POST['required_state_name'])
	project.region = RegionState.objects.get(stateprov_id=request.POST['required_state_name'])
	project.county = County.objects.get(county_id=request.POST['required_county_name'])		
	if 'required_presol' in request.POST:
		project.presolicitation = True #required_presol
	if 'required_incomplete' in request.POST:
		project.incomplete = True #required_incomplete
	if 'required_Sub_Contract':
		project.subcontractor=True #required_Sub_Contract

	# ''' Information filed '''
	if 'owner_solicitation_name' in request.POST:
		project.ownerprjname = project.name = request.POST['owner_solicitation_name']
	if 'bid_number' in request.POST:
		project.ownerprjno = request.POST['bid_number']
	if 'owner_location' in request.POST:
		project.location = Location.objects.get(location_id=request.POST['owner_location'])
	if 'low_value' in request.POST:
		project.lowvalue = request.POST['low_value']
	if 'high_value' in request.POST:
		project.highvalue = request.POST['high_value']
	if 'bond_percentage' in request.POST:
		project.bond = request.POST['bond_percentage']
	if 'pre_bid_date' in request.POST:
		project.prebiddate = request.POST['pre_bid_date']
	if 'pre_bid_time' in request.POST:
		project.prebidtime = request.POST['pre_bid_time']
	if 'mandatory' in request.POST:
		project.prebidmandatory = True
	if 'prequal_required' in request.POST:
		project.prequalification = True
	if 'contract_status' in request.POST:
		project.status = ProjectStatus.objects.get(pk=request.POST['contract_status'])

	# ''' NOTES '''
	project.scope = request.POST['notes_scope']
	project.reporternotes = request.POST['reporter_notes']
	project.outsidelink = request.POST.getlist('linkfield[]')
	project.notes = request.POST['notes_notes']
	project.plans = request.POST['reporter_notes']
	if 'no_adr' in request.POST:
		project.no_adr = True
	if 'plan_price' in request.POST:
		project.planprice = request.POST['plan_price']

	# ''' OWNER '''

	project.owner = Entity.objects.get(entity_id=request.POST['ownerid'])
	project.ownercontact = Contact.objects.get(contact_id=request.POST['ownercontactid'])

	# ''' ENGINEER '''
	project.engineer = Entity.objects.get(entity_id=request.POST['engineerid'])
	project.engineercontact = Contact.objects.get(contact_id=request.POST['engineercontactid'])

	# ''' PLANKeeper '''
	project.plan = Entity.objects.get(entity_id=request.POST['plankeeperid'])
	project.plancontact = Contact.objects.get(contact_id=request.POST['plankeepercontactid'])
	if 'plankeeperphone' in request.POST:
		project.planphone = request.POST['plankeeperphone']
	if 'plankeeperphoneext' in request.POST:
		project.planphoneext = request.POST['plankeeperphoneext']
	if 'plankeeperfax' in request.POST:
		project.planfax = request.POST['plankeeperfax']
	if 'plankeeperemail' in request.POST:
		project.planemail = request.POST['plankeeperemail']
	project.prime = Entity.objects.get(entity_id=request.POST['managerid'])
	project.primecontact = Contact.objects.get(contact_id=request.POST['managercontactid'])
	project.leadby = Employee.objects.get(username=request.user.username)
	project.reportby = Employee.objects.get(username=request.user.username)
	project.updatedby = Employee.objects.get(username=request.user.username)
	project.save()
	project.worktype.set(request.POST.getlist('worktypeid'))
	project.save()

	savesetaside(request.POST.getlist('setaside'),project.pk)
	return HttpResponse('project saved')