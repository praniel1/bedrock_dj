from django import template

register = template.Library()

@register.filter()
def bitwise_and(value, arg):
	value = int(value)
	arg = int(arg)
	return bool(value & arg)
	# return value+1

