from .models import *
from .forms import TermContractForm
from django.http import HttpResponse
import random

def savesetaside(setasides, projectpk):
	# return HttpResponse(setasides)
	for setaside in setasides:
		projectsetaside = ProjectSetAside()
		projectsetaside.prjid = projectpk
		projectsetaside.setasidetypeid = setaside
		projectsetaside.save()

def savetermcontract(request,pk):
	termcontract = request.POST.get('current_start')
	if (termcontract is not None) and (len(termcontract)>0):
		post_request = request.POST
		post_request._mutable = True
		post_request['id'] = pk
		form = TermContractForm(post_request)
		if form.is_valid():
			form.save()
		else:
			print(form.errors)

def edittermcontract(request,pk):
	termcontract = request.POST.get('current_start')
	if (termcontract is not None) and (len(termcontract)>0):
		post_request = request.POST
		post_request._mutable = True
		post_request['id'] = pk
		form = TermContractForm(instance=ProjectTermContract.objects.get(pk=pk),data=post_request)
		if form.is_valid():
			form.save()
		else:
			print(form.errors)

def deletesetaside(pk):
	setasides = ProjectSetAside.objects.filter(prjid=pk)
	setasides.delete()

def getadrrequest(post_request):
	if 'owneradrph' in post_request:
		adr_request_from_ph =  post_request['owner']
	elif 'engineeradrph' in post_request:
		adr_request_from_ph =  post_request['engineer']
	elif 'plankeeperadrph' in post_request:
		adr_request_from_ph = post_request['plan']
	else:
		adr_request_from_ph = 0

	if 'owneradrbt' in post_request:
		adr_request_from_bt = post_request['owner']
	elif 'engineeradrbt' in post_request:
		adr_request_from_bt = post_request['engineer']
	elif 'plankeeperadrbt' in post_request:
		adr_request_from_bt = post_request['plan']
	else:
		adr_request_from_bt = 0

	if 'owneradrca' in post_request:
		adr_request_from_ca = post_request['owner']
	elif 'engineeradrca' in post_request :
		adr_request_from_ca = post_request['engineer']
	elif 'plankeeperadrca' in post_request:
		adr_request_from_ca = post_request['plan']
	else:
		adr_request_from_ca = 0
	
	post_request['adr_request_from_ph'] = adr_request_from_ph
	post_request['adr_request_from_bt'] = adr_request_from_bt
	post_request['adr_request_from_ca'] = adr_request_from_ca
	return post_request

def preparedata(request):
	post_request = request.POST
	post_request._mutable = True
	post_request['region'] = RegionState.objects.get(stateprov=post_request['stateprov'])
	post_request['outsidelink'] = post_request.getlist('outsidelink')
	post_request['name'] = post_request['ownerprjname']
	post_request['leadby'] = post_request['reportby'] = post_request['updatedby'] = Employee.objects.get(username=request.user.username)
	post_request['codedid'] = str(int(random.uniform(0, 1) * 10000000000000000)) + "." + str(int(random.uniform(0, 1) * 10000000000000000))
	post_request = getadrrequest(post_request)
	post_request['tags'] = int(post_request['formprivate_federal'])
	if 'formsubcontract' in post_request:
		# post_request['formsubcontract'] = 0
		post_request['tags'] = post_request['tags'] | int(post_request['formsubcontract'])
	if 'term_contract' in post_request:
		post_request['tags'] = post_request['tags'] | int(post_request['term_contract'])
	if 'formpreqrequired' in post_request:
		# post_request['formpreqrequired'] = 0
		post_request['tags'] = post_request['tags'] | int(post_request['formpreqrequired'])
	return post_request

utilityconstructions = Worktype.objects.filter(worktypegroup=Worktypegroup.objects.get(name='Utility Construction & Rehab'))
BuildingTrades = Worktype.objects.filter(worktypegroup=Worktypegroup.objects.get(name='Building Trades'))
RoadpavingandBridges = Worktype.objects.filter(worktypegroup=Worktypegroup.objects.get(name='Roads, Paving, and Bridges'))
architecturalengineeringconsultings = Worktype.objects.filter(worktypegroup=Worktypegroup.objects.get(name='Architectural, Engineering, Consulting, Surveying'))
SpecialtyTrades = Worktype.objects.filter(worktypegroup=Worktypegroup.objects.get(name='Specialty Trades'))
OtherServices = Worktype.objects.filter(worktypegroup=Worktypegroup.objects.get(name='Other Services'))
ItemSaleLeaseTrades = Worktype.objects.filter(worktypegroup=Worktypegroup.objects.get(name='Items for Sale or Trade'))
ConstructionMaterials = Worktype.objects.filter(worktypegroup=Worktypegroup.objects.get(name='Construction Materials'))
PurchaseRentalLeaseofGoods = Worktype.objects.filter(worktypegroup=Worktypegroup.objects.get(name='Purchase, Rental or Lease of Goods'))
EquipmentMachineryVehicles = Worktype.objects.filter(worktypegroup=Worktypegroup.objects.get(name='Equipment, Machinery, Vehicles and Tools'))

bidtimes = ['9:00 AM','10:00 AM','11:00 AM','12:00 AM','1:00 PM','2:00 PM','3:00 PM','4:00 PM','5:00 PM']

# tablecolors = ['secondary','danger','primary','success','warning','info','warning','success','info','primary','secondary','danger','primary']
def convertformerror(formerrors):
	string = 'There are some errors with <br><ol>'
	for field in formerrors:
		string+= '<li>'+field+'</li>'
	string+='</ol>'
	# for key, value in formerrors.items:
	# 	string+= "key: "+key+" value"+value
	# return string