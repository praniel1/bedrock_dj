from django.contrib import admin
from . models import *
admin.site.register(Country)
admin.site.register(Stateprov)
admin.site.register(Location)
admin.site.register(EmployeeType)

admin.site.register(Entitytype)
admin.site.register(County)
admin.site.register(ProjectClass)
admin.site.register(RegionState)
admin.site.register(ProjectStatus)
admin.site.register(ProjectType)
admin.site.register(Sourcetype)

@admin.register(Entity)
class EntityAdmin(admin.ModelAdmin):
    list_display=['entity_id','entityname','entitytype']
    list_per_page = 5
    
@admin.register(Contact)
class ContactAdmin(admin.ModelAdmin):
    search_fields = ('contact_id',)
    list_display=['name','contact_id']
    list_per_page = 10

@admin.register(Employee)
class EmployeeAdmin(admin.ModelAdmin):
    list_display = ['username','is_staff','is_superuser']
    list_per_page = 10

@admin.register(Worktype)
class WorktypeAdmin(admin.ModelAdmin):
    list_display = ['worktype_id','workcode','name','active']
    list_per_page = 20

@admin.register(Worktypegroup)

class WorktypegroupAdmin(admin.ModelAdmin):
    list_display = ['worktypegroup_id','name','pricefactor']
    list_per_page = 20

class ProjectAdmin(admin.ModelAdmin):
    raw_id_fields = ('county','country','stateprov','owner','ownercontact','engineer','engineercontact','plan','plancontact','prime','primecontact',)
admin.site.register(Project, ProjectAdmin)


class ProjectTermContractAdmin(admin.ModelAdmin):
    list_display = ['id','current_start','current_stop']

admin.site.register(ProjectTermContract,ProjectTermContractAdmin)

class ProjectSetAsideAdmin(admin.ModelAdmin):
    list_display = ['psid','setasidetypeid','prjid']
admin.site.register(ProjectSetAside, ProjectSetAsideAdmin)