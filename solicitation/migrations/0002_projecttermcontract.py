# Generated by Django 3.1.4 on 2020-12-30 08:31

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('solicitation', '0001_initial'),
    ]

    operations = [
        migrations.CreateModel(
            name='ProjectTermContract',
            fields=[
                ('id', models.IntegerField(primary_key=True, serialize=False)),
                ('current_start', models.DateField()),
                ('current_stop', models.DateField()),
                ('orig_start', models.DateField()),
                ('orig_stop', models.DateField()),
                ('contract_term', models.IntegerField()),
                ('ext_count', models.IntegerField()),
                ('ext_length', models.IntegerField()),
                ('status', models.IntegerField(choices=[(0, 'Choose'), (1, 'Pending'), (2, 'Active'), (3, 'Completed')], default=0)),
            ],
        ),
    ]
