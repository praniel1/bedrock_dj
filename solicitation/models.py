from django.db import models
from django.contrib.auth.models import AbstractUser
import datetime
from django.utils import timezone


class Employee(AbstractUser):
	first_name = models.CharField(blank=True, max_length=30)
	last_name = models.CharField(blank=True, max_length=150)
	location = models.ForeignKey('Location', models.SET_NULL, blank=True, null=True, )
	date_joined = models.DateTimeField(default=timezone.now, blank=True)
	schedule = models.CharField(max_length=50, blank=True, null=True)
	hoursperweek = models.IntegerField(blank=True, null=True)
	nextvacation = models.DateField(blank=True, null=True)
	is_logged_in = models.BooleanField(default=0)
	username = models.CharField(unique=True, null=True, max_length=150)
	password = models.CharField(blank=True, null=True, max_length=128)
	last_login = models.DateTimeField(default=timezone.now, blank=True)
	current_login = models.DateTimeField(default=timezone.now, blank=True)  # for updating every few seconds
	street1 = models.CharField(max_length=50, blank=True, null=True)
	street2 = models.CharField(max_length=50, blank=True, null=True)
	city = models.CharField(max_length=30, blank=True, null=True)
	stateprov = models.ForeignKey('Stateprov', models.SET_NULL, blank=True, null=True, )
	zip = models.CharField(max_length=15, blank=True, null=True)
	country = models.ForeignKey('Country', models.SET_NULL, blank=True, null=True, )
	homephone = models.CharField(max_length=20, blank=True, null=True)
	cellphone = models.CharField(max_length=20, blank=True, null=True)
	fax = models.CharField(max_length=20, blank=True, null=True)
	email = models.CharField(blank=True, null=True, max_length=254)
	bidoemail = models.CharField(max_length=70, blank=True, null=True)
	aim = models.CharField(max_length=30, blank=True, null=True)
	contract = models.IntegerField(blank=True, null=True)
	coname = models.CharField(max_length=70, blank=True, null=True)
	contractdate = models.DateField(blank=True, null=True)
	website = models.CharField(max_length=70, blank=True, null=True)
	photo = models.TextField(blank=True, null=True)
	phototype = models.CharField(max_length=130, blank=True, null=True)
	jobdescr = models.TextField(blank=True, null=True)
	mgtnotes = models.TextField(blank=True, null=True)
	resdefsp = models.CharField(max_length=250, blank=True, null=True)
	salesdefaultsp = models.CharField(max_length=250, blank=True, null=True)
	bidsdefsp = models.CharField(max_length=250, blank=True, null=True)
	# subscriberid = models.PositiveIntegerField(blank=True, null=True)
	description = models.TextField(blank=True, null=True)
	newfaxes = models.IntegerField(blank=True, null=True)
	updatedby = models.IntegerField(blank=True, null=True)
	dtupdated = models.DateTimeField(blank=True, null=True)
	birthdate = models.DateField(blank=True, null=True)
	# birthdaycardid = models.IntegerField(blank=True, null=True)
	can_take_test = models.PositiveIntegerField(blank=True, null=True)
	#  companyid = models.PositiveIntegerField()
	gender = models.CharField(max_length=1, blank=True, null=True)
	#   shiftid = models.IntegerField()
	position = models.ManyToManyField('EmployeeType')
	is_staff = models.BooleanField(default=0)
	is_active = models.BooleanField(default=1)
	is_superuser = models.BooleanField(default=0)

	#   applicantid = models.PositiveIntegerField()
	# work_from_home = models.IntegerField()
	# production_pay = models.PositiveIntegerField()
	# supervisor = models.PositiveIntegerField(blank=True, null=True)
	# commission = models.FloatField(blank=True, null=True)
	# cts = models.CharField(max_length=5)
	# dev_prog = models.PositiveIntegerField()
	def __str__(self):  # Python 3: def __unicode__(self):
		return self.username


class Location(models.Model):
	location_id = models.AutoField(primary_key=True)
	name = models.CharField(max_length=50, blank=True, null=True)
	timezonename = models.CharField(max_length=50, blank=True, null=True)
	timezoneabbrev = models.CharField(max_length=5, blank=True, null=True)
	timezoneshift = models.IntegerField(blank=True, null=True)
	timezoneshiftmin = models.IntegerField(blank=True, null=True)
	active = models.BooleanField(default=1)

	def __str__(self):  # Python 3: def __unicode__(self):
		return self.name


class Stateprov(models.Model):
	stateprov_id = models.AutoField(primary_key=True)
	country = models.ForeignKey('Country', models.SET_NULL, blank=True, null=True, )
	abbrev = models.CharField(max_length=10, blank=True, null=True)
	name = models.CharField(max_length=50)
	active = models.BooleanField(default=1)
	baseprice = models.DecimalField(max_digits=10, decimal_places=2, blank=True, null=True)
	population = models.IntegerField(blank=True, null=True)
	hourshift = models.IntegerField()

	def __str__(self):  # Python 3: def __unicode__(self):
		return self.name


class Country(models.Model):
	country_id = models.AutoField(primary_key=True)
	abbrev = models.CharField(max_length=10, blank=True, null=True)
	name = models.CharField(max_length=50, blank=True, null=True)
	active = models.BooleanField(default=1)

	def __str__(self):  # Python 3: def __unicode__(self):
		return self.name


class EmployeeType(models.Model):
	position_id = models.AutoField(primary_key=True)
	name = models.CharField(max_length=50)
	active = models.BooleanField(default=1)

	def __str__(self):  # Python 3: def __unicode__(self):
		return self.name


class Project(models.Model):
	''' required '''
	prj_id = models.AutoField(primary_key=True)
	source = models.CharField(max_length=150, blank=True, null=True)
	country = models.ForeignKey('Country', models.SET_NULL, blank=True, null=True, )
	stateprov = models.ForeignKey('Stateprov', models.SET_NULL, blank=True, null=True, )
	county = models.ForeignKey('County', models.SET_NULL, blank=True, null=True, )
	biddate = models.DateField(blank=True, null=True)
	bidtime = models.CharField(max_length=25, blank=True, null=True)
	presolicitation = models.BooleanField(default=0)
	incomplete = models.BooleanField(default=0)
	tags = models.CharField(max_length=25, blank=True, null=True)
	classname = models.ForeignKey('ProjectClass', models.SET_NULL, blank=True, null=True, )  # Field renamed because it was a Python reserved word.
	sourcetype = models.ForeignKey('Sourcetype', models.SET_NULL, blank=True, null=True)
	# sourcetypeid = models.PositiveIntegerField(blank=True, null=True)

	''' information '''
	ownerprjname = models.CharField(max_length=150, blank=True, null=True)
	ownerprjno = models.CharField(max_length=50, blank=True, null=True)
	location = models.ForeignKey('Location', models.SET_NULL, blank=True, null=True, )
	lowvalue = models.PositiveIntegerField(blank=True, null=True)
	highvalue = models.PositiveIntegerField(blank=True, null=True)
	bond = models.CharField(max_length=150, blank=True, null=True)
	prebiddate = models.DateField(blank=True, null=True)
	prebidtime = models.CharField(max_length=25, blank=True, null=True)
	prebidmandatory = models.BooleanField(default=0)
	status = models.ForeignKey('ProjectStatus', models.SET_NULL, blank=True, null=True, )

	''' check boxes '''

	''' NOTES '''
	scope = models.TextField(blank=True, null=True)
	no_adr = models.BooleanField(default=0)
	notes = models.TextField(blank=True, null=True)
	reporternotes = models.TextField(blank=True, null=True)
	outsidelink = models.TextField(blank=True, null=True)
	plans = models.TextField(blank=True, null=True)
	planprice = models.DecimalField(max_digits=20, decimal_places=2, blank=True, null=True)

	''' SOLICITOR '''

	''' OWNER '''
	owner = models.ForeignKey('Entity', models.SET_NULL, blank=True, null=True, related_name='owner')
	ownercontact = models.ForeignKey('Contact', models.SET_NULL, blank=True, null=True, related_name='ownercontact')

	''' ENGINEER '''
	engineer = models.ForeignKey('Entity', models.SET_NULL, blank=True, null=True, related_name='engineer')
	engineercontact = models.ForeignKey('Contact', models.SET_NULL, blank=True, null=True,
										related_name='engineercontact')

	''' PLANKeeper '''
	plan = models.ForeignKey('Entity', models.SET_NULL, blank=True, null=True, related_name='plan')
	plancontact = models.ForeignKey('Contact', models.SET_NULL, blank=True, null=True, related_name='plancontact')
	planphone = models.CharField(max_length=35, blank=True, null=True)
	planphoneext = models.CharField(max_length=10, blank=True, null=True)
	planfax = models.CharField(max_length=20, blank=True, null=True)
	planemail = models.CharField(max_length=70, blank=True, null=True)

	''' prime contractor '''
	prime = models.ForeignKey('Entity', models.SET_NULL, blank=True, null=True, related_name='prime')
	primecontact = models.ForeignKey('Contact', models.SET_NULL, blank=True, null=True, related_name='primecontact')

	codedid = models.CharField(max_length=35, blank=True, null=True)
	leadby = models.ForeignKey('Employee', models.SET_NULL, blank=True, null=True, related_name='leadby')
	dtlead = models.DateTimeField(default=timezone.now, blank=True)
	reportby = models.ForeignKey('Employee', models.SET_NULL, blank=True, null=True, related_name='reportby')
	dtreport = models.DateTimeField(default=timezone.now, blank=True, null=True)
	updatedby = models.ForeignKey('Employee', models.SET_NULL, blank=True, null=True, )
	dtupdated = models.DateTimeField(default=timezone.now)
	dtlastupdate = models.DateTimeField(blank=True, null=True)
	# subscriberid = models.PositiveIntegerField(blank=True, null=True)
	name = models.CharField(max_length=150, blank=True, null=True)
	region = models.ForeignKey('RegionState', models.SET_NULL, blank=True, null=True)
	publishdate = models.DateTimeField(blank=True, null=True, default=timezone.now)
	freebie = models.BooleanField(default=0)
	projecttype = models.ForeignKey('ProjectType', models.SET_NULL, null=True)
	active = models.BooleanField(default=1)
	awardedto = models.PositiveIntegerField(blank=True, null=True)
	awardamount = models.DecimalField(max_digits=20, decimal_places=2, blank=True, null=True)
	privatenotes = models.TextField(blank=True, null=True)
	adr_request_from_ph = models.PositiveIntegerField(blank=True, null=True)
	adr_request_from_bt = models.PositiveIntegerField(blank=True, null=True)
	adr_request_from_ca = models.PositiveIntegerField(blank=True, null=True)
	updatenotes = models.TextField(blank=True, null=True)
	worktype = models.ManyToManyField('Worktype')
	# procusertypeid = models.PositiveSmallIntegerField()
	# procuserid = models.PositiveIntegerField()
	# procprivate = models.IntegerField()
	contract_end = models.DateField(blank=True, null=True)

	# solicitorid = models.PositiveIntegerField()
	def __str__(self):  # Python 3: def __unicode__(self):
		return str(self.prj_id)


class Entity(models.Model):
	entity_id = models.AutoField(primary_key=True)
	entitytype = models.ForeignKey('Entitytype', models.SET_NULL, blank=True, null=True, )
	active = models.BooleanField(default=1)
	contact = models.ForeignKey('Contact', models.SET_NULL, blank=True, null=True, )
	contacttitle = models.CharField(max_length=130, blank=True, null=True)
	entityname = models.CharField(max_length=130, blank=True, null=True)
	street1 = models.CharField(max_length=130, blank=True, null=True)
	street2 = models.CharField(max_length=50, blank=True, null=True)
	city = models.CharField(max_length=50, blank=True, null=True)
	county = models.ForeignKey('County', models.SET_NULL, blank=True, null=True, )
	stateprov = models.ForeignKey('Stateprov', models.SET_NULL, blank=True, null=True, )
	country = models.ForeignKey('Country', models.SET_NULL, blank=True, null=True, )
	zip = models.CharField(max_length=15, blank=True, null=True)
	phone = models.CharField(max_length=35, blank=True, null=True)
	phoneext = models.CharField(max_length=10, blank=True, null=True)
	phoneadd = models.CharField(max_length=20, blank=True, null=True)
	cellphone = models.CharField(max_length=20, blank=True, null=True)
	pager = models.CharField(max_length=20, blank=True, null=True)
	fax = models.CharField(max_length=20, blank=True, null=True)
	frequency = models.CharField(max_length=10, blank=True, null=True)
	email = models.TextField(blank=True, null=True)
	website = models.CharField(max_length=130, blank=True, null=True)
	newsurl = models.CharField(max_length=130, blank=True, null=True)
	procurementurl = models.CharField(max_length=130, blank=True, null=True)
	procurementurlnotes = models.CharField(max_length=100, blank=True, null=True)
	pcmtlastvisitdate = models.DateTimeField(blank=True, null=True)
	pcmtlastvisitempid = models.PositiveIntegerField(blank=True, null=True)
	project_data_on_website = models.BooleanField(default=0)
	procurlvisitcodes = models.CharField(max_length=50, blank=True, null=True)
	new = models.BooleanField(default=0)
	pending = models.BooleanField(default=0)
	is_parent = models.BooleanField(default=0)
	parent = models.PositiveIntegerField(blank=True, null=True)
	description = models.TextField(blank=True, null=True)
	datecreated = models.DateTimeField(blank=True, null=True)
	createdby = models.PositiveIntegerField(blank=True, null=True)
	dateupdated = models.DateTimeField(blank=True, null=True)
	updatedby = models.PositiveIntegerField(blank=True, null=True)
	notes = models.TextField(blank=True, null=True)
	prequalification = models.TextField(blank=True, null=True)
	reporternotes = models.TextField(blank=True, null=True)
	otcode = models.CharField(max_length=255, blank=True, null=True)
	tccode = models.CharField(max_length=10, blank=True, null=True)
	outsidefacilities = models.BooleanField(default=0)
	employees = models.PositiveIntegerField(blank=True, null=True)
	primecontractor = models.BooleanField(default=0)
	subcontractor = models.BooleanField(default=0)
	phone_not_found = models.BooleanField(default=0)
	website_not_found = models.BooleanField(default=0)
	hidden = models.BooleanField(default=0)
	has_source = models.BooleanField(default=0)
	other_source = models.TextField(blank=True, null=True)
	adr_request = models.BooleanField(default=1)
	adr_request_ph = models.PositiveIntegerField()
	adr_request_bt = models.PositiveIntegerField()
	adr_request_ca = models.PositiveIntegerField()
	adr_email = models.CharField(max_length=70, blank=True, null=True)
	adr_fax = models.CharField(max_length=20, blank=True, null=True)
	adr_phone = models.CharField(max_length=20, blank=True, null=True)
	adr_url_ph = models.CharField(max_length=130, blank=True, null=True)
	adr_url_bt = models.CharField(max_length=130, blank=True, null=True)
	adr_url_ca = models.CharField(max_length=130, blank=True, null=True)
	population = models.IntegerField(blank=True, null=True)
	latitude = models.DecimalField(max_digits=6, decimal_places=4, blank=True, null=True)
	longtitude = models.DecimalField(max_digits=7, decimal_places=4, blank=True, null=True)
	ams_send = models.PositiveIntegerField(default=0, blank=True, null=True)
	revenue = models.PositiveIntegerField(blank=True, null=True)
	asr = models.BooleanField(default=0)
	nosource = models.BooleanField(default=0)
	consentdecree = models.BooleanField(default=0)
	source_searched = models.BooleanField(default=0)
	corruption = models.BooleanField(default=0)
	status = models.BooleanField(default=0)
	ownercategory = models.BooleanField(default=0)
	source_manager_note = models.TextField(blank=True, null=True)
	worktype = models.ManyToManyField('Worktype')

	def __str__(self):  # Python 3: def __unicode__(self):
		return self.entityname


class Entitytype(models.Model):
	entitytype_id = models.AutoField(primary_key=True)
	typename = models.CharField(max_length=30, blank=True, null=True)
	description = models.CharField(max_length=250, blank=True, null=True)

	def __str__(self):  # Python 3: def __unicode__(self):
		return self.typename


class County(models.Model):
	county_id = models.AutoField(primary_key=True)
	stateprov = models.ForeignKey('Stateprov', models.SET_NULL, blank=True, null=True, )
	countyname = models.CharField(max_length=50)
	countytype = models.CharField(max_length=16)
	population = models.IntegerField(blank=True, null=True)
	baseprice = models.DecimalField(max_digits=10, decimal_places=2, blank=True, null=True)
	active = models.BooleanField(default=1)

	def __str__(self):  # Python 3: def __unicode__(self):
		return self.countyname


class Contact(models.Model):
	contact_id = models.AutoField(primary_key=True)
	name = models.CharField(max_length=130, blank=True, null=True)
	type = models.CharField(max_length=130, blank=True, null=True)
	phone = models.CharField(max_length=20, blank=True, null=True)
	phoneext = models.CharField(max_length=10, blank=True, null=True)
	cellphone = models.CharField(max_length=20, blank=True, null=True)
	pager = models.CharField(max_length=20, blank=True, null=True)
	fax = models.CharField(max_length=20, blank=True, null=True)
	email = models.TextField(blank=True, null=True)
	ams_send = models.BooleanField(default=1)
	active = models.BooleanField(default=0)

	def __str__(self):  # Python 3: def __unicode__(self):
		return self.name


class ProjectClass(models.Model):
	classname_id = models.AutoField(primary_key=True)
	classname = models.CharField(max_length=130, blank=True, null=True)

	def __str__(self):  # Python 3: def __unicode__(self):
		return self.classname


class RegionState(models.Model):
	region_id = models.AutoField(primary_key=True)
	regioncode = models.CharField(max_length=130, blank=True, null=True)
	stateprov = models.ForeignKey('Stateprov', models.SET_NULL, blank=True, null=True, )

	def __str__(self):  # Python 3: def __unicode__(self):
		return self.regioncode


class ProjectStatus(models.Model):
	status_id = models.AutoField(primary_key=True)
	status = models.CharField(max_length=130, blank=True, null=True)
	active = models.BooleanField(default=1)

	def __str__(self):  # Python 3: def __unicode__(self):
		return self.status


class ProjectType(models.Model):
	projecttype_id = models.AutoField(primary_key=True)
	name = models.CharField(max_length=130, blank=True, null=True)
	active = models.BooleanField(default=1)

	def __str__(self):  # Python 3: def __unicode__(self):
		return self.name


class Worktype(models.Model):
	worktype_id = models.AutoField(primary_key=True)
	workcode = models.CharField(unique=True, max_length=3)
	name = models.CharField(max_length=150, blank=True, null=True)
	pricefactor = models.DecimalField(max_digits=10, decimal_places=2, blank=True, null=True)
	active = models.BooleanField(default=1)
	worktypegroup = models.ManyToManyField('Worktypegroup')

	def __str__(self):  # Python 3: def __unicode__(self):
		return self.name


class Worktypegroup(models.Model):
	worktypegroup_id = models.AutoField(primary_key=True)
	name = models.CharField(max_length=200, blank=True, null=True)
	pricefactor = models.DecimalField(max_digits=10, decimal_places=2, blank=True, null=True)
	active = models.BooleanField(default=1)

	def __str__(self):  # Python 3: def __unicode__(self):
		return self.name


class Sourcetype(models.Model):
	sourcetypeid = models.AutoField(primary_key=True)
	name = models.CharField(max_length=100)
	active = models.BooleanField(default=True)
	listorder = models.IntegerField(blank=True, null=True)
	nonutilrate = models.DecimalField(max_digits=10, decimal_places=2, blank=True, null=True)
	utilrate = models.DecimalField(blank=True, null=True, max_digits=10, decimal_places=2)

	def __str__(self):
		return self.name


class ProjectSetAside(models.Model):
	psid = models.AutoField(primary_key=True)
	prjid = models.IntegerField()
	setasidetypeid = models.IntegerField()
	def __str__(self):
		return str(self.setasidetypeid)



class ProjectTermContract(models.Model):
	id = models.IntegerField(primary_key=True)
	current_start = models.DateField()
	current_stop = models.DateField()
	orig_start = models.DateField()
	orig_stop = models.DateField()
	contract_term = models.IntegerField()
	ext_count = models.IntegerField()
	ext_length = models.IntegerField()
	STATUS_CHOICES = [
		(0, 'Choose'),
		(1,'Pending'),
		(2,'Active'),
		(3,'Completed'),
	]
	status = models.IntegerField(choices = STATUS_CHOICES, default=0)
	def __str__(self):
		return str(self.id)