from django.shortcuts import render
from django.http import HttpResponse
from dashpage.models import Employee, ProjectClass, Worktype, Sourcetype, Entitytype, Stateprov, Country,Worktypegroup,RegionState

def index(request):
	employees = Employee.objects.all()
	projectclass = ProjectClass.objects.all()
	worktypes = Worktype.objects.all()
	sources = Sourcetype.objects.all()
	entitytypes = Entitytype.objects.all()
	states = Stateprov.objects.all()
	countries = Country.objects.all()
	worktypegroups = Worktypegroup.objects.all()
	regions = RegionState.objects.all()
	context = {
	'employees':employees,
	'projectclass':projectclass,
	'worktypes':worktypes,
	'sources':sources,
	'entitytypes':entitytypes,
	'states':states,
	'countries':countries,
	'worktypegroups':worktypegroups,
	'regions':regions,
	}
	return render(request,'centrallookup/index.html', context)
