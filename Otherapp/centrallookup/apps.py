from django.apps import AppConfig


class CentrallookupConfig(AppConfig):
    name = 'centrallookup'
