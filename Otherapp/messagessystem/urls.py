from django.urls import path
from . import views
app_name ='messagessystem'

urlpatterns = [
	path('', views.index, name='index'),
]