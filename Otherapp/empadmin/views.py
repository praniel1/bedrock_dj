from django.shortcuts import render
from dashpage.models import Employee

def index(request):
	context={
		'employees':Employee.objects.all()
	}
	return render(request, 'empadmin/index.html', context)