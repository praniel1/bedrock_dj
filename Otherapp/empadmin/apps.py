from django.apps import AppConfig


class EmpadminConfig(AppConfig):
    name = 'empadmin'
