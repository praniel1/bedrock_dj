# This is an auto-generated Django model module.
# You'll have to do the following manually to clean this up:
#   * Rearrange models' order
#   * Make sure each model has one field with primary_key=True
#   * Make sure each ForeignKey and OneToOneField has `on_delete` set to the desired behavior
#   * Remove `managed = False` lines if you wish to allow Django to create, modify, and delete the table
# Feel free to rename the models, but don't rename db_table values or field names.
from django.db import models


class AuthGroup(models.Model):
    name = models.CharField(unique=True, max_length=150)

    class Meta:
        managed = False
        db_table = 'auth_group'


class AuthGroupPermissions(models.Model):
    group = models.ForeignKey(AuthGroup, models.DO_NOTHING)
    permission = models.ForeignKey('AuthPermission', models.DO_NOTHING)

    class Meta:
        managed = False
        db_table = 'auth_group_permissions'
        unique_together = (('group', 'permission'),)


class AuthPermission(models.Model):
    name = models.CharField(max_length=255)
    content_type = models.ForeignKey('DjangoContentType', models.DO_NOTHING)
    codename = models.CharField(max_length=100)

    class Meta:
        managed = False
        db_table = 'auth_permission'
        unique_together = (('content_type', 'codename'),)


class Contact(models.Model):
    contactid = models.AutoField(primary_key=True)
    entityid = models.PositiveIntegerField()
    name = models.CharField(max_length=130, blank=True, null=True)
    type = models.CharField(max_length=130, blank=True, null=True)
    phone = models.CharField(max_length=20, blank=True, null=True)
    phoneext = models.CharField(max_length=10, blank=True, null=True)
    cellphone = models.CharField(max_length=20, blank=True, null=True)
    pager = models.CharField(max_length=20, blank=True, null=True)
    fax = models.CharField(max_length=20, blank=True, null=True)
    email = models.TextField(blank=True, null=True)
    typeid = models.PositiveSmallIntegerField()
    ams_send = models.PositiveIntegerField()
    active = models.IntegerField()

    class Meta:
        managed = False
        db_table = 'contact'


class County(models.Model):
    countyid = models.AutoField(primary_key=True)
    stateprovid = models.IntegerField()
    countyname = models.CharField(max_length=50)
    countytype = models.CharField(max_length=16)
    population = models.IntegerField(blank=True, null=True)
    baseprice = models.DecimalField(max_digits=10, decimal_places=2, blank=True, null=True)
    active = models.PositiveIntegerField()

    class Meta:
        managed = False
        db_table = 'county'


class DashpageCountry(models.Model):
    country_id = models.AutoField(primary_key=True)
    abbrev = models.CharField(max_length=10, blank=True, null=True)
    name = models.CharField(max_length=50, blank=True, null=True)
    active = models.IntegerField(blank=True, null=True)
    menuorder = models.IntegerField(blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'dashpage_country'


class DashpageEmployee(models.Model):
    first_name = models.CharField(max_length=30)
    last_name = models.CharField(max_length=150)
    date_joined = models.DateTimeField()
    schedule = models.CharField(max_length=50, blank=True, null=True)
    hoursperweek = models.IntegerField(blank=True, null=True)
    nextvacation = models.DateField(blank=True, null=True)
    username = models.CharField(unique=True, max_length=150, blank=True, null=True)
    password = models.CharField(max_length=128, blank=True, null=True)
    last_login = models.DateTimeField()
    street1 = models.CharField(max_length=50, blank=True, null=True)
    street2 = models.CharField(max_length=50, blank=True, null=True)
    city = models.CharField(max_length=30, blank=True, null=True)
    zip = models.CharField(max_length=15, blank=True, null=True)
    homephone = models.CharField(max_length=20, blank=True, null=True)
    cellphone = models.CharField(max_length=20, blank=True, null=True)
    fax = models.CharField(max_length=20, blank=True, null=True)
    email = models.CharField(max_length=254, blank=True, null=True)
    bidoemail = models.CharField(max_length=70, blank=True, null=True)
    aim = models.CharField(max_length=30, blank=True, null=True)
    contract = models.IntegerField(blank=True, null=True)
    coname = models.CharField(max_length=70, blank=True, null=True)
    contractdate = models.DateField(blank=True, null=True)
    website = models.CharField(max_length=70, blank=True, null=True)
    photo = models.TextField(blank=True, null=True)
    phototype = models.CharField(max_length=130, blank=True, null=True)
    jobdescr = models.TextField(blank=True, null=True)
    mgtnotes = models.TextField(blank=True, null=True)
    resdefsp = models.CharField(max_length=250, blank=True, null=True)
    salesdefaultsp = models.CharField(max_length=250, blank=True, null=True)
    bidsdefsp = models.CharField(max_length=250, blank=True, null=True)
    description = models.TextField(blank=True, null=True)
    newfaxes = models.IntegerField(blank=True, null=True)
    updatedby = models.IntegerField(blank=True, null=True)
    dtupdated = models.DateTimeField(blank=True, null=True)
    birthdate = models.DateField(blank=True, null=True)
    can_take_test = models.PositiveIntegerField(blank=True, null=True)
    gender = models.CharField(max_length=1, blank=True, null=True)
    is_staff = models.IntegerField()
    is_active = models.IntegerField()
    is_superuser = models.IntegerField()
    country = models.ForeignKey(DashpageCountry, models.DO_NOTHING, blank=True, null=True)
    location = models.ForeignKey('DashpageLocation', models.DO_NOTHING, blank=True, null=True)
    position = models.ForeignKey('DashpageEmployeetype', models.DO_NOTHING, blank=True, null=True)
    stateprov = models.ForeignKey('DashpageStateprov', models.DO_NOTHING, blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'dashpage_employee'


class DashpageEmployeeGroups(models.Model):
    employee = models.ForeignKey(DashpageEmployee, models.DO_NOTHING)
    group = models.ForeignKey(AuthGroup, models.DO_NOTHING)

    class Meta:
        managed = False
        db_table = 'dashpage_employee_groups'
        unique_together = (('employee', 'group'),)


class DashpageEmployeeUserPermissions(models.Model):
    employee = models.ForeignKey(DashpageEmployee, models.DO_NOTHING)
    permission = models.ForeignKey(AuthPermission, models.DO_NOTHING)

    class Meta:
        managed = False
        db_table = 'dashpage_employee_user_permissions'
        unique_together = (('employee', 'permission'),)


class DashpageEmployeetype(models.Model):
    position_id = models.AutoField(primary_key=True)
    name = models.CharField(max_length=50)
    active = models.IntegerField()

    class Meta:
        managed = False
        db_table = 'dashpage_employeetype'


class DashpageLocation(models.Model):
    location_id = models.AutoField(primary_key=True)
    name = models.CharField(max_length=50, blank=True, null=True)
    timezonename = models.CharField(max_length=50, blank=True, null=True)
    timezoneabbrev = models.CharField(max_length=5, blank=True, null=True)
    timezoneshift = models.IntegerField(blank=True, null=True)
    timezoneshiftmin = models.IntegerField(blank=True, null=True)
    active = models.PositiveIntegerField()

    class Meta:
        managed = False
        db_table = 'dashpage_location'


class DashpageStateprov(models.Model):
    stateprov_id = models.AutoField(primary_key=True)
    abbrev = models.CharField(max_length=10, blank=True, null=True)
    name = models.CharField(max_length=50)
    active = models.IntegerField(blank=True, null=True)
    baseprice = models.DecimalField(max_digits=10, decimal_places=2, blank=True, null=True)
    population = models.IntegerField(blank=True, null=True)
    hourshift = models.IntegerField()
    country = models.ForeignKey(DashpageCountry, models.DO_NOTHING, blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'dashpage_stateprov'


class DjangoAdminLog(models.Model):
    action_time = models.DateTimeField()
    object_id = models.TextField(blank=True, null=True)
    object_repr = models.CharField(max_length=200)
    action_flag = models.PositiveSmallIntegerField()
    change_message = models.TextField()
    content_type = models.ForeignKey('DjangoContentType', models.DO_NOTHING, blank=True, null=True)
    user = models.ForeignKey(DashpageEmployee, models.DO_NOTHING)

    class Meta:
        managed = False
        db_table = 'django_admin_log'


class DjangoContentType(models.Model):
    app_label = models.CharField(max_length=100)
    model = models.CharField(max_length=100)

    class Meta:
        managed = False
        db_table = 'django_content_type'
        unique_together = (('app_label', 'model'),)


class DjangoMigrations(models.Model):
    app = models.CharField(max_length=255)
    name = models.CharField(max_length=255)
    applied = models.DateTimeField()

    class Meta:
        managed = False
        db_table = 'django_migrations'


class DjangoSession(models.Model):
    session_key = models.CharField(primary_key=True, max_length=40)
    session_data = models.TextField()
    expire_date = models.DateTimeField()

    class Meta:
        managed = False
        db_table = 'django_session'


class Entity(models.Model):
    entityid = models.AutoField(primary_key=True)
    entitytypeid = models.PositiveIntegerField()
    active = models.IntegerField()
    contact = models.CharField(max_length=130, blank=True, null=True)
    contacttitle = models.CharField(max_length=130, blank=True, null=True)
    entityname = models.CharField(max_length=130, blank=True, null=True)
    street1 = models.CharField(max_length=130, blank=True, null=True)
    street2 = models.CharField(max_length=50, blank=True, null=True)
    city = models.CharField(max_length=50, blank=True, null=True)
    county = models.CharField(max_length=50, blank=True, null=True)
    stateprovid = models.PositiveIntegerField(blank=True, null=True)
    countryid = models.PositiveIntegerField(blank=True, null=True)
    zip = models.CharField(max_length=15, blank=True, null=True)
    phone = models.CharField(max_length=35, blank=True, null=True)
    phoneext = models.CharField(max_length=10, blank=True, null=True)
    phoneadd = models.CharField(max_length=20, blank=True, null=True)
    cellphone = models.CharField(max_length=20, blank=True, null=True)
    pager = models.CharField(max_length=20, blank=True, null=True)
    fax = models.CharField(max_length=20, blank=True, null=True)
    frequency = models.CharField(max_length=10, blank=True, null=True)
    email = models.TextField(blank=True, null=True)
    website = models.CharField(max_length=130, blank=True, null=True)
    newsurl = models.CharField(max_length=130, blank=True, null=True)
    procurementurl = models.CharField(max_length=130, blank=True, null=True)
    procurementurlnotes = models.CharField(max_length=100, blank=True, null=True)
    pcmtlastvisitdate = models.DateTimeField(blank=True, null=True)
    pcmtlastvisitempid = models.PositiveIntegerField(blank=True, null=True)
    project_data_on_website = models.IntegerField()
    procurlvisitcodes = models.CharField(max_length=50, blank=True, null=True)
    new = models.IntegerField()
    pending = models.IntegerField()
    is_parent = models.IntegerField()
    parent = models.PositiveIntegerField(blank=True, null=True)
    description = models.TextField(blank=True, null=True)
    datecreated = models.DateTimeField(blank=True, null=True)
    createdby = models.PositiveIntegerField(blank=True, null=True)
    dateupdated = models.DateTimeField(blank=True, null=True)
    updatedby = models.PositiveIntegerField(blank=True, null=True)
    notes = models.TextField(blank=True, null=True)
    reporternotes = models.TextField(blank=True, null=True)
    otcode = models.CharField(max_length=255, blank=True, null=True)
    tccode = models.CharField(max_length=10, blank=True, null=True)
    outsidefacilities = models.IntegerField()
    employees = models.PositiveIntegerField(blank=True, null=True)
    primecontractor = models.PositiveIntegerField()
    subcontractor = models.PositiveIntegerField()
    phone_not_found = models.IntegerField()
    website_not_found = models.IntegerField()
    countyid = models.IntegerField(blank=True, null=True)
    hidden = models.IntegerField()
    has_source = models.IntegerField()
    other_source = models.TextField(blank=True, null=True)
    adr_request = models.IntegerField()
    adr_request_ph = models.PositiveIntegerField()
    adr_request_bt = models.PositiveIntegerField()
    adr_request_ca = models.PositiveIntegerField()
    adr_email = models.CharField(max_length=70, blank=True, null=True)
    adr_fax = models.CharField(max_length=20, blank=True, null=True)
    adr_phone = models.CharField(max_length=20, blank=True, null=True)
    adr_url_ph = models.CharField(max_length=130, blank=True, null=True)
    adr_url_bt = models.CharField(max_length=130, blank=True, null=True)
    adr_url_ca = models.CharField(max_length=130, blank=True, null=True)
    population = models.IntegerField(blank=True, null=True)
    latitude = models.DecimalField(max_digits=6, decimal_places=4, blank=True, null=True)
    longtitude = models.DecimalField(max_digits=7, decimal_places=4, blank=True, null=True)
    ams_send = models.PositiveIntegerField(blank=True, null=True)
    entityownerid = models.PositiveIntegerField()
    revenue = models.PositiveIntegerField(blank=True, null=True)
    asr = models.IntegerField()
    nosource = models.IntegerField()
    consentdecree = models.IntegerField()
    source_searched = models.IntegerField()
    corruption = models.IntegerField()
    status = models.PositiveIntegerField()
    ownercategory = models.PositiveIntegerField()
    prequalification = models.TextField(blank=True, null=True)
    source_manager_note = models.TextField(blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'entity'


class EntityWorktype(models.Model):
    entityid = models.PositiveIntegerField()
    workcode = models.CharField(max_length=3)
    generated = models.IntegerField()

    class Meta:
        managed = False
        db_table = 'entity_worktype'


class Entitytype(models.Model):
    entitytypeid = models.AutoField(primary_key=True)
    typename = models.CharField(max_length=30, blank=True, null=True)
    description = models.CharField(max_length=250, blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'entitytype'


class PrjWorktype(models.Model):
    prjid = models.PositiveIntegerField()
    workcode = models.CharField(max_length=3)
    worktypeid = models.PositiveIntegerField()

    class Meta:
        managed = False
        db_table = 'prj_worktype'


class Project(models.Model):
    prjid = models.AutoField(primary_key=True)
    ownerprjno = models.CharField(max_length=50, blank=True, null=True)
    ownerprjname = models.CharField(max_length=150, blank=True, null=True)
    codedid = models.CharField(max_length=35, blank=True, null=True)
    ownerid = models.PositiveIntegerField(blank=True, null=True)
    ownercontactid = models.PositiveIntegerField(blank=True, null=True)
    engineerid = models.PositiveIntegerField(blank=True, null=True)
    engineercontactid = models.PositiveIntegerField(blank=True, null=True)
    planid = models.PositiveIntegerField(blank=True, null=True)
    plancontactid = models.PositiveIntegerField(blank=True, null=True)
    planphone = models.CharField(max_length=35, blank=True, null=True)
    planphoneext = models.CharField(max_length=10, blank=True, null=True)
    planfax = models.CharField(max_length=20, blank=True, null=True)
    planemail = models.CharField(max_length=70, blank=True, null=True)
    planprice = models.DecimalField(max_digits=20, decimal_places=2, blank=True, null=True)
    primeid = models.PositiveIntegerField(blank=True, null=True)
    primecontactid = models.PositiveIntegerField(blank=True, null=True)
    leadby = models.PositiveIntegerField(blank=True, null=True)
    dtlead = models.DateTimeField(blank=True, null=True)
    reportby = models.PositiveIntegerField(blank=True, null=True)
    dtreport = models.DateTimeField(blank=True, null=True)
    updatedby = models.PositiveIntegerField(blank=True, null=True)
    dtupdated = models.DateTimeField(blank=True, null=True)
    dtlastupdate = models.DateTimeField(blank=True, null=True)
    source = models.CharField(max_length=150, blank=True, null=True)
    sourcetypeid = models.PositiveIntegerField(blank=True, null=True)
    subscriberid = models.PositiveIntegerField(blank=True, null=True)
    class_field = models.CharField(db_column='class', max_length=15, blank=True, null=True)  # Field renamed because it was a Python reserved word.
    presolicitation = models.IntegerField()
    name = models.CharField(max_length=150, blank=True, null=True)
    region = models.CharField(max_length=3, blank=True, null=True)
    stateprovid = models.PositiveIntegerField(blank=True, null=True)
    county = models.CharField(max_length=50, blank=True, null=True)
    countryid = models.PositiveIntegerField(blank=True, null=True)
    location = models.TextField(blank=True, null=True)
    publishdate = models.DateTimeField(blank=True, null=True)
    biddate = models.DateField(blank=True, null=True)
    bidtime = models.CharField(max_length=25, blank=True, null=True)
    prebiddate = models.DateField(blank=True, null=True)
    prebidtime = models.CharField(max_length=25, blank=True, null=True)
    prebidmandatory = models.IntegerField()
    bond = models.CharField(max_length=150, blank=True, null=True)
    scope = models.TextField(blank=True, null=True)
    plans = models.TextField(blank=True, null=True)
    notes = models.TextField(blank=True, null=True)
    reporternotes = models.TextField(blank=True, null=True)
    lowvalue = models.PositiveIntegerField(blank=True, null=True)
    highvalue = models.PositiveIntegerField(blank=True, null=True)
    freebie = models.IntegerField()
    projecttypeid = models.PositiveIntegerField()
    active = models.IntegerField()
    awardedto = models.PositiveIntegerField(blank=True, null=True)
    awardamount = models.DecimalField(max_digits=20, decimal_places=2, blank=True, null=True)
    incomplete = models.IntegerField()
    statusid = models.IntegerField(blank=True, null=True)
    privatenotes = models.TextField(blank=True, null=True)
    countyid = models.IntegerField(blank=True, null=True)
    adr_request_from_ph = models.PositiveIntegerField(blank=True, null=True)
    adr_request_from_bt = models.PositiveIntegerField(blank=True, null=True)
    adr_request_from_ca = models.PositiveIntegerField(blank=True, null=True)
    updatenotes = models.TextField(blank=True, null=True)
    outsidelink = models.TextField(blank=True, null=True)
    tags = models.PositiveIntegerField()
    procusertypeid = models.PositiveSmallIntegerField()
    procuserid = models.PositiveIntegerField()
    procprivate = models.IntegerField()
    no_adr = models.IntegerField()
    contract_end = models.DateField()
    solicitorid = models.PositiveIntegerField()

    class Meta:
        managed = False
        db_table = 'project'


class Projectclass(models.Model):
    classname = models.CharField(max_length=15)

    class Meta:
        managed = False
        db_table = 'projectclass'
