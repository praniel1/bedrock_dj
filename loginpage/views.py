from django.shortcuts import render, redirect
from django.contrib.auth import authenticate, login
from django.contrib import messages
from solicitation.models import Employee
from django.http import HttpResponse
from datetime import datetime
from pytz import timezone
def index(request):
	if request.user.is_authenticated:
		return redirect('dashpage:index')
	if request.method == 'POST':
		username = request.POST['username']
		userpassword = request.POST['userpassword']
		Employee = authenticate(username=username,password=userpassword)
		if Employee is not None:
			login(request, Employee)
			Employee.last_login = datetime.now().strftime('%Y-%m-%d %H:%M:%S.000000')
			Employee.save()
			return redirect('dashpage:index')
		else:
			messages.success(request,"Credentials do not match")
			return render(request,'loginpage/index.html')
	else:
		return render(request,'loginpage/index.html')

def registerusers(request):
	if request.method !='POST':
		return render(request,'loginpage/register.html')
	else:
		firstname = request.POST['firstname']
		lastname = request.POST['lastname']
		email = request.POST['email']
		username = request.POST['username']
		password = request.POST['password']
		user = Employee.objects.create_user(first_name=firstname,last_name=lastname,email=email,username=username,password=password)
		user.save()
		return HttpResponse(user)