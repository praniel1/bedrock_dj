from django.contrib import admin
from django.urls import path,include
from . import views
app_name='dashpage'
urlpatterns = [
    path('',views.index, name='index'),
    path('logout', views.logout_user, name='logout'),
    path('getonlineusers',views.get_online_users, name='get_online_users'),
]
