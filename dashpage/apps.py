from django.apps import AppConfig


class DashpageConfig(AppConfig):
    name = 'dashpage'
