from django.shortcuts import render, redirect
from django.http import HttpResponse
from django.conf import settings
from django.contrib.auth import authenticate, logout, login
from django.contrib.sessions.models import Session
from datetime import datetime
from pytz import timezone
from solicitation.models import Location, Entity, Entitytype, Employee, Contact, Project, ProjectClass, Sourcetype
from django.core.paginator import Paginator


def index(request):
    if request.user.is_authenticated:
        # return HttpResponse()
        username = request.user.username
        user = Employee.objects.get(username=username)
        user.is_logged_in = True
        user.save()
        bhutan_thimpu = datetime.now(timezone('Asia/Thimphu')).strftime('%d/%h/%Y %I:%M %p')
        us_east = datetime.now(timezone('US/Eastern')).strftime('%d/%h/%Y %I:%M %p')
        us_central = datetime.now(timezone('US/Central')).strftime('%d/%h/%Y %I:%M %p')
        us_mountain = datetime.now(timezone('US/Mountain')).strftime('%d/%h/%Y %I:%M %p')
        us_west = datetime.now(timezone('US/Alaska')).strftime('%d/%h/%Y %I:%M %p')
        india = datetime.now(timezone('Asia/Kolkata')).strftime('%d/%h/%Y %I:%M %p')
        canada_british_columbia = datetime.now(timezone('Canada/Pacific')).strftime('%d/%h/%Y %I:%M %p')
        canada_nova_scotia = datetime.now(timezone('Canada/Eastern')).strftime('%d/%h/%Y %I:%M %p')
        uk_london = datetime.now(timezone('Europe/London')).strftime('%d/%h/%Y %I:%M %p')
        phillipines = datetime.now(timezone('Asia/Manila')).strftime('%d/%h/%Y %I:%M %p')
        context = {
            'us_east': us_east,
            'us_central': us_central,
            'bhutan_thimpu': bhutan_thimpu,
            'india': india,
            'canada_british_columbia': canada_british_columbia,
            'canada_nova_scotia': canada_nova_scotia,
            'us_mountain': us_mountain,
            'us_west': us_west,
            'uk_london': uk_london,
            'phillipines': phillipines,
            'username': username,
        }
        return render(request, 'dashpage/dashpage.html', context)
    else:
        return redirect('loginpage:index')


def logout_user(request):
    user = Employee.objects.get(username=request.user.username)
    user.is_logged_in = False
    user.save()
    logout(request)
    return redirect('/')


def get_online_users(request):
    users = Employee.objects.values('username', 'last_login', 'current_login', 'is_logged_in').order_by('username')
    now_time = datetime.now().strftime('%Y-%m-%d %H:%M:%S')
    online_users = []
    updateuserlogin(request.user.username)
    for user in users:
        is_logged_in = user['is_logged_in']
        if is_logged_in:
            current_login = user['current_login'].strftime('%Y-%m-%d %H:%M:%S')
            duration = datetime.strptime(now_time, '%Y-%m-%d %H:%M:%S') - datetime.strptime(current_login,
                                                                                            '%Y-%m-%d %H:%M:%S')
            if duration.days == 0:
                # return HttpResponse(duration.seconds)
                if duration.seconds < 7:
                    userduration = user['current_login'] - user['last_login']
                    user['duration'] = str(userduration).split(".")[0]
                    online_users.append(user)
    return render(request, 'dashpage/components/online_users.html', {'online_users': online_users})


def updateuserlogin(username):
    now = datetime.now().strftime('%Y-%m-%d %H:%M:%S.000000')
    user = Employee.objects.get(username=username)
    user.current_login = now
    user.save()
