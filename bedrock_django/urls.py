"""bedrock_django URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/2.2/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django.contrib import admin
from django.urls import path,include

urlpatterns = [
    path('admin/', admin.site.urls),
    path('',include('loginpage.urls'), name='loginpage'),
    path('dashpage',include('dashpage.urls'), name='dashpage'),
    path('solicitation/', include('solicitation.urls'), name='solicitation'),
    # path('central-look-up', include('centrallookup.urls'), name='centrallookup'),
    # path('entityadd', include('entityadd.urls'), name='entityadd'),
    # path('empadmin', include('empadmin.urls'), name='empadmin'),
    # path('messages-system/', include('messagessystem.urls'), name='messages'),
]
